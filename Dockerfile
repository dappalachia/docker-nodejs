FROM alpine:3.8

RUN apk add --no-cache \
        build-base \
        git \
        nodejs \
        npm \
        python \
        && \
    npm i npm@latest -g

WORKDIR /app

ONBUILD COPY package.json /app/package.json

ONBUILD RUN npm install

CMD ["npm", "start"]