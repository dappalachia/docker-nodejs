# Base nodejs docker image

This is a base nodejs docker image. It is an ONBUILD image. It is not meant to be used directly, but as a base for nodejs images.

## Usage

The most basic usage is to create a valid `package.json` file.

```json
{
    "name": "dappalachia-nodejs-example",
    "description": "Example nodejs container package",
    "author": "Robert Davis <bo@aWholeFarm.com>",
    "version": "0.0.1",
    "scripts": {
        "start": "node --version"
    }
}
```

And a simple docker file in the same directory.

```
FROM dappalachia/nodejs
```

Run docker build with a valid image name to build your image.

```bash
docker build -t dappalachia/nodejs-example .
```

You can then run your app like so.

```bash
docker run dappalachia/nodejs-example
```

There is a slightly more complete example with a makefile [here](example/).