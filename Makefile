.PHONY: build shell

IMAGE_BASE = dappalachia
IMAGE = nodejs
MY_PWD = $(shell pwd)

all: build

build:
ifdef NOCACHE
	docker build --no-cache -t $(IMAGE_BASE)/$(IMAGE) -f $(MY_PWD)/Dockerfile $(MY_PWD)
else
	docker build -t $(IMAGE_BASE)/$(IMAGE) -f $(MY_PWD)/Dockerfile $(MY_PWD)
endif

push:
	docker push $(IMAGE_BASE)/$(IMAGE)

shell:
	docker run -it --rm $(IMAGE_BASE)/$(IMAGE) sh
